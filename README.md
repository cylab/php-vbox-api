# php-vbox-api

[![pipeline status](https://gitlab.cylab.be/cylab/php-vbox-api/badges/master/pipeline.svg)](https://gitlab.cylab.be/cylab/php-vbox-api/commits/master)
[![coverage report](https://gitlab.cylab.be/cylab/php-vbox-api/badges/master/coverage.svg)](https://gitlab.cylab.be/cylab/php-vbox-api/commits/master)

A PHP library to drive VirtualBox. The API allows you to do stuff like:

```php
$vbox = new VBox($username, $password);

// a single .ova may contain multiple machines
$vms = $vbox->import("/path/to/image.ova");
$vm = $vms[0];

$vm->setMemorySize(512); // MB
$vm->setCPUCount(2);

$adapter = $vm->getNetworkAdapter(0);
$adapter->setAttachmentType(NetworkAdapter::ATTACHEMENT_BRIDGED);
$adapter->setBridgedInterface("eno1");

$vm->up();
```

This library is actually a **wrapper for the SOAP webservice of VirtualBox**. You can find a copy of the complete SOAP webservice documentation in the [documentation](./documentation/) directory.

## Installation

The library itself is best installed using composer:

```bash
composer require cylab-be/php-vbox-api
```

## VirtualBox Web Service

You of course have to **[download and install VirtualBox](https://www.virtualbox.org/wiki/Downloads)**.

In VirtualBox, each virtual machine is a separate user process. So it is usually best to **create a dedicated user for running your VM's** :

```bash
sudo adduser vbox
```

Then add this new user to the *vbox* group so it will be allowed to run virtual machines :

```bash
sudo adduser vbox vbox
```

Finally, you have to **activate the VirtualBox Web Service**.

Therefore, you have to create the file **/etc/default/virtualbox** and indicate which user should be used to run your machines:

```
VBOXWEB_USER=vbox
```

You can now start the VirtualBox Web Service:

```bash
sudo service vboxweb-service restart
```

## Usage

**Connecting to VirtualBox**

```php
$vbox = new VBox("vbox", "passord-of-vbox-user");
```

**List all machines**

```php
$vms = $vbox->allVMs();
foreach ($vms as $vm) {
  echo $vm->getName() . "\n";
  echo $vm->getUUID() . "\n";
}
```

**Modify a VM**

```php
$vm = $vbox->findVM("name or UUID");

// do a clean shutdown
$vm->halt();
$vm->setMemory(2048);
$vm->setCPUCount(4);
$vm->up();

// do a hard shutdown
$vm->kill();

// destroy the VM
$vm->destroy();
```

**Take a screenshot**

```php
$vm->up();
sleep(5);
$img = $vm->takeScreenshot();
file_put_contents('/path/to/screenshot.png', $img);
```
