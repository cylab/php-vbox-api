<?php
namespace Cylab\Vbox;

/**
 * Description of Appliance
 *
 * @author Thibault Debatty
 */
class Appliance extends VBoxObject
{

    private $file;

    /**
     * Read an OVA file.
     * @param String $file path to OVA file
     * @return Progress
     */
    public function read(string $file) : Progress
    {
        $this->file = $file;

        return new Progress(
            $this->call("IAppliance_read", array("file" => $file)),
            $this->getVBox()
        );
    }

    public function interpret()
    {
        $this->callRaw("IAppliance_interpret");
    }

    public function importMachines()
    {
        return new Progress(
            $this->call("IAppliance_importMachines"),
            $this->getVBox()
        );
    }

    /**
     *
     * @return array<\Cylab\Vbox\VM>
     */
    public function getMachines() : array
    {
        $response = $this->callRaw("IAppliance_getMachines");
        if (! isset($response->returnval)) {
            throw new \Exception("No VM was imported. Is the OVA corrupted? ("
                    . $this->file . ")");
        }
        $uuids = (array) $response->returnval;
        $machines = array();
        foreach ($uuids as $uuid) {
            $machines[] = $this->getVBox()->findVM($uuid);
        }
        return $machines;
    }

    /**
     *
     * @return \Cylab\Vbox\VirtualSystemDescription[]
     */
    public function getVirtualSystemDescriptions()
    {
        $uuids = (array) $this->call("IAppliance_getVirtualSystemDescriptions");
        $vsds = [];
        foreach ($uuids as $uuid) {
            $vsds[] = new VirtualSystemDescription($uuid, $this->getVBox());
        }

        return $vsds;
    }

    public function write(string $path) : Progress
    {
        return new Progress(
            $this->call("IAppliance_write", [
                "format" => "ovf-2.0",
                "options" => [],
                "path" => $path
                ]),
            $this->getVBox()
        );
    }
}
