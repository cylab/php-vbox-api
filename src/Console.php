<?php
namespace Cylab\Vbox;

/**
 * A Console allows to control VM execution.
 * A Console object gets created when a machine has been locked for a particular
 * session.
 *
 * https://www.virtualbox.org/sdkref/interface_i_console.html
 * @author Thibault Debatty
 */
class Console extends VBoxObject
{
    public function getDisplay() : Display
    {
        return new Display(
            $this->call("IConsole_getDisplay"),
            $this->getVBox()
        );
    }

    public function getGuest() : Guest
    {
        return new Guest(
            $this->call("IConsole_getGuest"),
            $this->getVBox()
        );
    }
}
