<?php

namespace Cylab\Vbox;

/**
 * A virtual DHCP Server that can be attached to an internal network.
 * https://www.virtualbox.org/sdkref/interface_i_d_h_c_p_server.html
 * @author Thibault Debatty
 */
class DHCPServer extends VBoxObject
{

    /**
     *
     * @param string $ip
     * @param string $mask
     * @param string $from
     * @param string $to
     */
    public function setConfiguration(string $ip, string $mask, string $from, string $to)
    {
        $this->callRaw("IDHCPServer_setConfiguration", [
            "IPAddress" => $ip,
            "networkMask" => $mask,
            "FromIPAddress" => $from,
            "ToIPAddress" => $to
        ]);
    }

    public function start($network)
    {
        $this->call("IDHCPServer_start", [
            "networkName" => $network,
            "trunkName" => "eno01",
            "trunkType" => "whatever"
        ]);
    }
    
    /**
     * Get the name of corresponding internal network.
     * @return string
     */
    public function network() : string
    {
        return $this->call("IDHCPServer_getNetworkName");
    }
    
    /**
     * Name of attached network
     * @return string
     */
    public function name() : string
    {
        return $this->network();
    }
    
    public function ipAddress() : string
    {
        return $this->call("IDHCPServer_getIPAddress");
    }
    
    public function networkMask() : string
    {
        return $this->call("IDHCPServer_getNetworkMask");
    }
    
    public function lowerIP() : string
    {
        return $this->call("IDHCPServer_getLowerIP");
    }
    
    public function upperIP() : string
    {
        return $this->call("IDHCPServer_getUpperIP");
    }

    public function enable(bool $enable = true)
    {
        $this->callRaw("IDHCPServer_setEnabled", ["enabled" => $enable]);
    }

    public function isEnabled() : bool
    {
        return (bool) $this->call("IDHCPServer_getEnabled");
    }

    public function delete()
    {
        $this->getVBox()->removeDHCPServer($this);
    }
    
    /**
     *
     * @return array(VM)
     */
    public function machines() : array
    {
        $network_name = $this->name();
        
        $machines = [];
        foreach ($this->getVBox()->allVMs() as $vm) {
            foreach (range(0, 7) as $i) {
                $adapter = $vm->getNetworkAdapter($i);
                if ($adapter->getAttachmentType() == "Internal" && $adapter->getInternalNetwork() == $network_name) {
                    $machines[] = $vm;
                    // check next VM
                    break 1;
                }
            }
        }
        return $machines;
    }
}
