<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cylab\Vbox;

/**
 * Description of Display
 *
 * @author tibo
 */
class Display extends VBoxObject
{
    
    
    public function takeScreenshot(
        int $screen,
        int $width,
        int $height,
        string $format
    ) {
        return $this->call(
            "IDisplay_takeScreenshotToArray",
            [
                    "screenId" => $screen,
                    "width" => $width,
                    "height" => $height,
                    "bitmapFormat" => $format
            ]
        );
    }
    
    public function getScreenResolution(int $screen = 0)
    {
        return $this->callRaw(
            "IDisplay_getScreenResolution",
            ["screenId" => $screen]
        );
    }

    public function computeResolution($max_width, $max_height)
    {
        $acutal_res = $this->getScreenResolution();
        $actual_width = $acutal_res->width;
        $actual_height = $acutal_res->height;
        
        if ($actual_width <= $max_width && $actual_height <= $max_height) {
            // the screen is smaller then max dimensions
            return [
                "width" => $actual_width,
                "height" => $actual_height];
        }

        $max_ratio = floatval($max_width) / $max_height;
        $actual_ratio = floatval($actual_width) / $actual_height;

        if ($actual_ratio > $max_ratio) {
            // $max_width will be the limiting factor...
            return [
                "width" => $max_width,
                "height" => $actual_height * $max_width / $actual_width];
        }
        
        // $max_height will be the limiting factor...
        return [
            "height" => $max_height,
            "width" => $actual_width * $max_height / $actual_height];
    }
}
