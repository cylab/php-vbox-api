<?php

namespace Cylab\Vbox;

/**
 * Description of Progress
 *
 * @author Thibault Debatty
 */
class Error extends VBoxObject
{

    /**
     * Get textual description of error.
     * @return string
     */
    public function description()
    {
        return $this->call("IVirtualBoxErrorInfo_getText");
    }
}
