<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cylab\Vbox;

/**
 * Represents a group of VM's
 *
 * @author Thibault Debatty
 */
class Group extends VBoxObject
{

    public function __construct($name, VBox $vbox)
    {
        parent::__construct(self::normalize($name), $vbox);
    }

    /**
     *
     * @return String the name of this group (actually, the UUID).
     */
    public function getName()
    {
        return $this->getUUID();
    }

    /**
     * Get all VM's in this group.
     * @param boolean $recursive include VM's from subgroups
     * @return \Cylab\Vbox\VM[]
     */
    public function getVMs($recursive = true)
    {
        $vms = array();
        $vbox = $this->getVBox();
        foreach ($vbox->allVMs() as $vm) {
            // each VM may belong to multiple groups => check them all
            foreach ($vm->getGroups() as $group) {
                $group = self::normalize($group);

                if ($recursive
                        && self::startsWith($group, $this->getName())) {
                    $vms[] = $vm;

                    // go to next VM
                    break;
                }

                if (!$recursive
                        &&  $group === $this->getName()) {
                    $vms[] = $vm;

                    // go to next VM
                    break;
                }
            }
        }
        return $vms;
    }

    /**
     * Boot all VM's in this group.
     */
    public function up($recursive = true)
    {
        foreach ($this->getVMs($recursive) as $vm) {
            $vm->up();
            sleep(5);
        }
    }

    /**
     * Helper function to normalize the name of a group (add leading and
     * trailing slash).
     * @param String $name
     * @return String normalized name
     */
    public static function normalize($name)
    {
        $name = "/" . trim($name, "/ ") . "/";

        if ($name == "//") {
            return "/";
        }

        return $name;
    }

    /**
     * Check if $string starts with $prefix.
     * @param String $string
     * @param String $prefix
     * @return boolean
     */
    public static function startsWith($string, $prefix)
    {
        return (substr($string, 0, strlen($prefix)) === $prefix);
    }
}
