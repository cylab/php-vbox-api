<?php

namespace Cylab\Vbox;

/**
 * Guest system.
 *
 * @author tibo
 */
class Guest extends VBoxObject
{

    public function getAdditionsVersion()
    {
        return $this->call("IGuest_getAdditionsVersion");
    }

    /**
     * https://www.virtualbox.org/sdkref/_virtual_box_8idl.html#ae0ae133b7ff9bcca841ce04ac7c6586c
     */
    public function getFacilities()
    {
        return $this->callRaw("IGuest_getFacilities");
    }

    public function internalGetStatistics()
    {
        return $this->callRaw("IGuest_internalGetStatistics");
    }
}
