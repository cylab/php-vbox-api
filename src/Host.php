<?php

namespace Cylab\Vbox;

/**
 * Description of Host
 *
 * @author tibo
 */
class Host extends VBoxObject
{

    public function processorCoreCount() : int
    {
        return $this->call("IHost_getProcessorCoreCount");
    }

    /**
     * Total memory (in MB)
     * @return int
     */
    public function memorySize() : int
    {
        return $this->call("IHost_getMemorySize");
    }

    /**
     * Available memory (in MB)
     * @return int
     */
    public function memoryAvailable() : int
    {
        return $this->call("IHost_getMemoryAvailable");
    }

    public function memoryUsed() : int
    {
        return $this->memorySize() - $this->memoryAvailable();
    }

    /**
     *
     * @return array(HostNetworkInterface)
     */
    public function networkInterfaces() : array
    {
        $interfaces = [];
        foreach ($this->call("IHost_getNetworkInterfaces") as $uuid) {
            $interfaces[] = new HostNetworkInterface($uuid, $this->getVBox());
        }
        return $interfaces;
    }
}
