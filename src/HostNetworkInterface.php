<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cylab\Vbox;

/**
 * Description of HostNetworkInterface
 *
 * @author tibo
 */
class HostNetworkInterface extends VBoxObject
{

    public function name() : string
    {
        return $this->call("IHostNetworkInterface_getName");
    }

    /**
     * Get the IP address of this interface.
     * @return string
     */
    public function ip() : string
    {
        return $this->call("IHostNetworkInterface_getIPAddress");
    }

    /**
     * Get the MAC address of the interface.
     * @return string
     */
    public function mac() : string
    {
        return $this->call("IHostNetworkInterface_getHardwareAddress");
    }

    /**
     * Get the status of this interface : Up, Down or Unknown.
     * @return string
     */
    public function status() : string
    {
        return $this->call("IHostNetworkInterface_getStatus");
    }
}
