<?php

namespace Cylab\Vbox;

/**
 * Description of ManagedObject
 *
 * @author Thibault Debatty
 */
abstract class ManagedObject
{

    protected $uuid;

    public function __construct($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * Do a SOAP call and return "returnval".
     * @param string $method
     * @param array $request
     * @return mixed
     */
    protected function call($method, $request = array())
    {
        $response = $this->callRaw($method, $request);

        if (!isset($response->returnval)) {
            throw new \Exception(
                "Method $method does not return a 'returnval'. "
                    . "Maybe you should use 'callRaw' "
                    . "or check the documentation: "
                . "https://www.virtualbox.org/sdkref/annotated.html "
                    . "Received response : " . json_encode($response)
            );
        }
        return $response->returnval;
    }


    /**
     * Do a SOAP call and return raw response.
     * Useful for some specific calls that do not return a "returnval".
     * @param string $method
     * @param array $request
     * @return mixed
     */
    protected function callRaw(string $method, $request = array())
    {
        // !! getClient must be called first (it will also define uuid)
        $client = $this->getClient();
        $request["_this"] = $this->uuid;
        return $client->__soapCall($method, array($request));
    }

    /**
     * Get the SoapClient to perform requests.
     *
     * @return \SoapClient
     */
    abstract protected function getClient(): \SoapClient;

    /**
     *
     * @return String
     */
    public function getUUID()
    {
        return $this->uuid;
    }

    public function releaseRemote()
    {
        $this->call("IManagedObjectRef_release");
    }
}
