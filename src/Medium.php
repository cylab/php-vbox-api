<?php

namespace Cylab\Vbox;

/**
 * A Medium (like a virtual hard disk).
 *
 * @link https://www.virtualbox.org/sdkref/interface_i_medium.html
 *
 * @author Thibault Debatty
 */
class Medium extends VBoxObject
{

    protected function getMutable() : Medium
    {
        throw new \Exception("Not implemented!");
    }
    
    /**
     * Location of the storage unit holding medium data.
     *
     * @link https://www.virtualbox.org/sdkref/interface_i_medium.html#add0200d560c7f06631d6e979ebd8510a
     * @return string
     */
    public function getLocation() : string
    {
        return $this->call("IMedium_getLocation");
    }

    /**
     *
     * @link https://www.virtualbox.org/sdkref/interface_i_medium.html#a1fbf41d84302b52c2392454c60a415b8
     * @return string
     */
    public function getName() : string
    {
        return $this->call("IMedium_getName");
    }

    /**
     * Physical size of the storage unit used to hold medium data (in bytes).
     *
     * @return int
     */
    public function getSize() : int
    {
        return $this->call("IMedium_getSize");
    }
    
    /**
     * Physical size of the storage unit used to hold medium data.
     *
     * E.g. 1.23GB
     *
     * @return string
     */
    public function getSizeForHumans() : string
    {
        return self::sizeForHumans($this->getSize());
    }
    
    /**
     * Logical size of this medium (in bytes), as reported to the guest OS running
     * inside the virtual machine this medium is attached to.
     *
     *
     * @link https://www.virtualbox.org/sdkref/interface_i_medium.html#aa36e4be53f0b493acb063287c8649ca3
     * @return int
     */
    public function getLogicalSize() : int
    {
        return $this->call("IMedium_getLogicalSize");
    }
    
    /**
     * Human readable logical size of this medium, as reported to the guest OS running
     * inside the virtual machine this medium is attached to. E.g. 10GB
     *
     * @return string
     */
    public function getLogicalSizeForHumans() : string
    {
        return self::sizeForHumans($this->getLogicalSize());
    }
    
    /**
     * Convert Byte size to something human readable.
     *
     * @param int $size in Bytes
     * @param int $precision
     * @return string
     */
    public static function sizeForHumans($size, int $precision = 2) : string
    {
        for ($i = 0; ($size / 1024) > 0.9; $i++, $size /= 1024) {
        }
        return round($size, $precision) . ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'][$i];
    }

    /**
     * Type (role) of this medium.
     *
     * Normal, Immutable, Writethgough etc.
     *
     * @link https://www.virtualbox.org/sdkref/_virtual_box_8idl.html#ab4abb9d1b9e44b997d1d2a809e66191c
     * @link https://www.virtualbox.org/sdkref/interface_i_medium.html#ab5430e5698b129a24c36d01fbc554cea
     *
     * @return string
     */
    public function getType() : string
    {
        return $this->call("IMedium_getType");
    }
    
    /**
     * Kind of device : DVD, Floppy or HardDisk
     *
     * @link https://www.virtualbox.org/sdkref/interface_i_medium.html#a01b3d1aa474671a6f6efc7d768e02603
     *
     * @return string
     */
    public function getDeviceType() : string
    {
        return $this->call("IMedium_getDeviceType");
    }
    
    /**
     * Starts creating a hard disk storage unit in the background.
     *
     * @param int $logicalSize in Bytes
     * @param array $variant
     * @return Progress
     */
    public function createBaseStorage(int $logicalSize, array $variant) : Progress
    {
        return new Progress(
            $this->call("IMedium_createBaseStorage", [
                    "logicalSize" => $logicalSize,
                    "variant" => $variant
                ]),
            $this->getVBox()
        );
    }
    
    /**
     * Array of UUIDs of all machines this medium is attached to.
     *
     * @return array<string>
     */
    public function getMachineIds() : array
    {
        return $this->callRaw("IMedium_getMachineIds")->returnval ?? [];
    }
    
    /**
     * Array of VMs this medium is attached to.
     * @return array<VM>
     */
    public function getMachines() : array
    {
        $r = [];
        foreach ($this->getMachineIds() as $id) {
            $r[] = $this->getVBox()->findVM($id);
        }
        return $r;
    }
        
    
    public function isAttached() : bool
    {
        return count($this->getMachineIds()) !== 0;
    }
    
    /**
     * Starts deleting the storage unit of this medium.
     *
     * The medium must not be attached to any known virtual machine and must not have any known child media, otherwise
     *  the operation will fail.
     *
     * @link https://www.virtualbox.org/sdkref/interface_i_medium.html#a94d118ff3f2ad05638d4c705532f0dce
     *
     * @return Progress
     */
    public function delete() : Progress
    {
        return new Progress($this->call("IMedium_deleteStorage"), $this->getVBox());
    }
    
    /**
     * Closes this medium.
     *
     * The medium must not be attached to any known virtual machine and must not have any known child media, otherwise
     * the operation will fail. When the medium is successfully closed, it is removed from the list of registered
     * media, but its storage unit is not deleted.
     *
     * @link https://www.virtualbox.org/sdkref/interface_i_medium.html#a8e006ecc7b5bc8d9d8cb281b237a8e23
     * @return void
     */
    public function close() : void
    {
        $this->callRaw("IMedium_close");
    }
}
