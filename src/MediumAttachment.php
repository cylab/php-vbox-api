<?php

namespace Cylab\Vbox;

/**
 * A MediumAttachment links a VM, a Medium (like a virtual hard disk) and a
 * storage controller.
 *
 * Warning: unlike other classes, MediumAttachment has no UUID!
 *
 * https://www.virtualbox.org/sdkref/interface_i_medium_attachment.html
 *
 * @author Thibault Debatty
 */
class MediumAttachment
{

    private $medium;
    private $controller;
    private $port;
    private $device;
    private $type;
    
    /**
     *
     * @var VM
     */
    private $vm;

    public function __construct($obj, VM $vm)
    {
        $this->vm = $vm;
        $this->medium = $obj->medium;
        $this->controller = $obj->controller;
        $this->port = $obj->port;
        $this->device = $obj->device;
        $this->type = $obj->type;
    }

    public function hasMedium() : bool
    {
        return $this->medium != "";
    }


    public function getMedium() : Medium
    {
        return new Medium($this->medium, $this->getVM()->getVBox());
    }

    /**
     * Get StorageController.
     *
     * @return StorageController
     */
    public function getController() : StorageController
    {
        return $this->getVM()->getStorageControllerByName($this->controller);
    }

    /**
     * Storage controller port to attach the device.
     *
     * For an IDE controller, 0 specifies
     * the primary controller and 1 specifies the secondary controller. For a SCSI
     * controller, this ranges from 0 to 15; for a SATA controller, from 0 to 29;
     * for an SAS controller, from 0 to 7.
     *
     * @return int
     */
    public function getPort() : int
    {
        return $this->port;
    }

    /**
     * Device slot number of this attachment.
     *
     * This is only relevant for IDE controllers, for which 0 specifies the master
     * device and 1 specifies the slave device. For all other controller types,
     * this is 0.
     *
     * @return int
     */
    public function getDevice() : int
    {
        return $this->device;
    }

    /**
     * E.g. DVD or HardDisk
     * @return string
     */
    public function getType() : string
    {
        return $this->type;
    }

    public function getVM() : VM
    {
        return $this->vm;
    }
}
