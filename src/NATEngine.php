<?php

namespace Cylab\Vbox;

/**
 * Description of NATEngine
 * https://www.virtualbox.org/sdkref/interface_i_n_a_t_engine.html
 *
 * @author Thibault Debatty
 */
class NATEngine extends VMComponent
{

    /**
     *
     * @var NetworkAdapter
     */
    private $adapter;

    public function __construct($uuid, VM $vm, NetworkAdapter $adapter)
    {
        parent::__construct($uuid, $vm);
        $this->adapter = $adapter;
    }

    /**
     *
     * @param \Cylab\Vbox\NATRedirect $nat_redirect
     * @return $this
     */
    public function addRedirect(NATRedirect $nat_redirect)
    {

        $this->getMutable()->callRaw("INATEngine_addRedirect", [
            "name" => $nat_redirect->name,
            "proto" => $nat_redirect->proto,
            "hostIP" => $nat_redirect->hostIP,
            "hostPort" => $nat_redirect->hostPort,
            "guestIP" => $nat_redirect->guestIP,
            "guestPort" => $nat_redirect->guestPort
        ]);

        $this->save();

        return $this;
    }

    /**
     *
     * @return NATRedirect[]
     */
    public function getRedirects()
    {
        $strings = $this->call("INATEngine_getRedirects");
        $redirects = [];
        foreach ($strings as $string) {
            $components = explode(",", $string);
            $redirects[] = new NATRedirect(
                (int) $components[3],
                (int) $components[5],
                $components[1],
                $components[2],
                $components[4],
                $components[0]
            );
        }

        return $redirects;
    }

    /**
     * The name of the NAT network.
     * @return String
     */
    public function getNetwork()
    {
        return $this->call("INATEngine_getNetwork");
    }

    public function setNetwork($network)
    {
        $this->getMutable()->callRaw("INATEngine_setNetwork", [
            "network" => $network
        ]);
        $this->save();
    }

    /**
     * return NATEngine
     * @throws \Exception
     */
    public function getMutable()
    {
        return $this->getVM()->getMutable()
                ->getNetworkAdapter($this->adapter->getSlot())
                ->getNATEngine();
    }
}
