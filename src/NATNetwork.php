<?php

namespace Cylab\Vbox;

/**
 * NATNetwork
 * https://www.virtualbox.org/sdkref/interface_i_n_a_t_network.html
 *
 * @author Thibault Debatty
 */
class NATNetwork extends VBoxObject
{
    public function name() : string
    {
        return $this->call("INATNetwork_getNetworkName");
    }
    
    public function remove() : void
    {
        $this->getVBox()->removeNATNetwork($this);
    }
    
    public function getNetwork() : string
    {
        return $this->call("INATNetwork_getNetwork");
    }
    
    /**
     * Set CIDR subnet used for this network
     * E.g. 192.168.155.0/24
     *
     * https://www.virtualbox.org/sdkref/interface_i_n_a_t_network.html#a80ae833bc20ae268e41cfd96e7b08856
     * @param string $net
     * @return void
     */
    public function setNetwork(string $net) : void
    {
        $this->callRaw("INATNetwork_setNetwork", [
            "network" => $net
        ]);
    }
    
    public function gateway() : string
    {
        return $this->call("INATNetwork_getGateway");
    }
    
    public function enableDHCPServer(bool $enable)
    {
        $this->call("INATNetwork_setNeedDhcpServer", [
            "needDhcpServer" => $enable
        ]);
    }
    
    public function hasDHCPServer() : bool
    {
        return $this->call("INATNetwork_getNeedDhcpServer");
    }
    
    public function isEnabled() : bool
    {
        return $this->call("INATNetwork_getEnabled");
    }
    
    public function enable(bool $enable)
    {
        $this->call("INATNetwork_setEnabled", [
            "enable" => $enable
        ]);
    }
}
