<?php

namespace Cylab\Vbox;

/**
 * Description of NATRedirect
 *
 * @author Thibault Debatty
 */
class NATRedirect
{
    public $name = "";
    public $proto = "TCP";
    public $hostIP = "";
    public $hostPort = 2222;
    public $guestIP = "";
    public $guestPort = 22;

    public function __construct(
        int $hostPort,
        int $guestPort,
        string $proto = "TCP",
        string $hostIP = "",
        string $guestIP = "",
        string $name = ""
    ) {
        $this->name = $name;
        $this->proto = $proto;
        $this->hostIP = $hostIP;
        $this->hostPort = $hostPort;
        $this->guestIP = $guestIP;
        $this->guestPort = $guestPort;
    }
}
