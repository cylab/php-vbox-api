<?php

namespace Cylab\Vbox;

/**
 * Description of NetworkAdapter
 * https://www.virtualbox.org/manual/ch06.html
 * https://www.virtualbox.org/sdkref/interface_i_network_adapter.html
 *
 * @author Thibault Debatty
 */
class NetworkAdapter extends VMComponent
{

    const ATTACHEMENT_NAT = "NAT";
    const ATTACHEMENT_BRIDGED = "Bridged";
    const ATTACHEMENT_INTERNAL = "Internal";

    /**
     * Get attachment type (bridged, NAT, etc.)
     * @return String
     */
    public function getAttachmentType()
    {
        return $this->call("INetworkAdapter_getAttachmentType");
    }

    /**
     * Set the attachment type (NAT, Bridged, HostOnly,etc.).
     * @param string $type
     */
    public function setAttachmentType(string $type)
    {
        $this->getMutable()->callRaw(
            "INetworkAdapter_setAttachmentType",
            array("attachmentType" => $type)
        );
        $this->save();
    }

    /**
     * Set attachement as NAT and return NATEngine
     * (allows to chain with addRedirect)
     * @return \Cylab\Vbox\NATEngine
     */
    public function nat()
    {
        $this->setAttachmentType(self::ATTACHEMENT_NAT);
        return $this->getNATEngine();
    }

    public function bridge($interface)
    {
        $this->setAttachmentType(self::ATTACHEMENT_BRIDGED);
        $this->setBridgedInterface($interface);
    }

    /**
     * Get the host interface on which this adapter is bridged.
     * @return String
     */
    public function getBridgedInterface()
    {
        return $this->call("INetworkAdapter_getBridgedInterface");
    }

    /**
     * Set the host interface on which this adapters is bridged.
     * @param string $interface
     */
    public function setBridgedInterface(string $interface)
    {
        $this->getMutable()->callRaw(
            "INetworkAdapter_setBridgedInterface",
            array("bridgedInterface" => $interface)
        );
        $this->save();
    }

    /**
     * Get the internal network which this adapter is connected.
     * @return String
     */
    public function getInternalNetwork()
    {
        return $this->call("INetworkAdapter_getInternalNetwork");
    }

    /**
     * Set the internal network on which this adapters is connected.
     * @param string $network
     */
    public function setInternalNetwork(string $network)
    {
        $this->getMutable()->callRaw(
            "INetworkAdapter_setInternalNetwork",
            array("internalNetwork" => $network)
        );
        $this->save();
    }

    public function setNATNetwork(string $network)
    {
        $this->getMutable()->callRaw(
            "INetworkAdapter_setNATNetwork",
            array("NATNetwork" => $network)
        );
        $this->save();
    }
    
    public function getNATNetwork() : string
    {
        return $this->call("INetworkAdapter_getNATNetwork");
    }

    public function isEnabled() : bool
    {
        return (bool) $this->call("INetworkAdapter_getEnabled");
    }

    /**
     * (De)activate this network interface.
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled)
    {
        $this->getMutable()->callRaw(
            "INetworkAdapter_setEnabled",
            array("enabled" => $enabled)
        );
        $this->save();
    }

    /**
     * Get the MAC address of the adapter.
     * @return string
     */
    public function getMACAddress() : string
    {
        return $this->formatMACAddress(
            $this->call("INetworkAdapter_getMACAddress")
        );
    }

    public function formatMACAddress(string $address) : string
    {
        $mac = preg_replace("/[^a-fA-F0-9]/", '', $address);
        $mac = (str_split($mac, 2));

        if (!(count($mac) == 6)) {
            throw new \Exception("Invalid MAC format: $address");
        }
     
        return $mac[0]. ":" . $mac[1] . ":" . $mac[2]. ":" . $mac[3] . ":" . $mac[4]. ":" . $mac[5];
    }

    /**
     * Get the IP address of this adapter.
     * @return string
     */
    public function getIPAddress()
    {
        $slot = $this->getSlot();
        return $this->getVM()->call(
            "IMachine_getGuestPropertyValue",
            array("property" => "/VirtualBox/GuestInfo/Net/$slot/V4/IP")
        );
    }

    /**
     *
     * @return int the slot (position) of this adapter
     */
    public function getSlot()
    {
        return $this->call("INetworkAdapter_getSlot");
    }

    /**
     *
     * @return NetworkAdapter
     */
    public function getMutable()
    {
        return $this->getVM()->getMutable()->getNetworkAdapter($this->getSlot());
    }

    public function getNATEngine()
    {
        return new NATEngine(
            $this->call("INetworkAdapter_getNATEngine"),
            $this->getVM(),
            $this
        );
    }

    /**
     * The name of the network to which this interface is attached.
     * @return string
     */
    public function getNetworkName()
    {
        $attachment = $this->getAttachmentType();

        switch ($attachment) {
            case self::ATTACHEMENT_BRIDGED:
                return $this->getBridgedInterface();

            case self::ATTACHEMENT_INTERNAL:
                return $this->getInternalNetwork();

            case self::ATTACHEMENT_NAT:
                return $this->getNATNetwork();
        }

        return "<unknown>";
    }
}
