<?php

namespace Cylab\Vbox;

/**
 * Description of Progress
 *
 * @author Thibault Debatty
 */
class Progress extends VBoxObject
{
    public function waitForCompletion($timeout = -1)
    {
        $this->callRaw("IProgress_waitForCompletion", array("timeout" => $timeout));

        if ($this->getResultCode() !== 0) {
            throw new \Exception("Operation failed: "
                    . $this->getError()->description());
        }
    }

    /**
     * Check if the operation monitored by this Progess has completed.
     * @return bool
     */
    public function isCompleted()
    {
        return $this->call("IProgress_getCompleted");
    }

    /**
     * Get error description.
     * @return Error
     */
    public function getError()
    {
        return new Error(
            $this->call("IProgress_getErrorInfo"),
            $this->getVBox()
        );
    }

    /**
     * Get result code.
     * @return int
     */
    public function getResultCode() : int
    {
        if (! $this->isCompleted()) {
            throw new \Exception("Invalide state: progress must be completed "
                    . "before checking resultCode!");
        }
        return $this->call("IProgress_getResultCode");
    }
}
