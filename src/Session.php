<?php

namespace Cylab\Vbox;

/**
 * Description of Session
 *
 * @author Thibault Debatty
 */
class Session extends VBoxObject
{
    public function unlockMachine()
    {
        $this->callRaw("ISession_unlockMachine");
    }


    /**
     * Get the currently locked machine, which is writable.
     * @return \Cylab\Vbox\VM
     */
    public function getMachine() : VM
    {
        return new VM(
            $this->call("ISession_getMachine"),
            $this->getVBox()
        );
    }

    public function getState()
    {
        return $this->call("ISession_getState");
    }

    public function getConsole() : Console
    {
        return new Console(
            $this->call("ISession_getConsole"),
            $this->getVBox()
        );
    }
}
