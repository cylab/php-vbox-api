<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cylab\Vbox;

/**
 * Description of StorageController
 *
 * @author tibo
 */
class StorageController extends VMComponent
{

    protected function getMutable() : StorageController
    {
        return $this->getVM()->getMutable()->getStorageControllerByName(
            $this->getName()
        );
    }

    public function getName() : string
    {
        return $this->call("IStorageController_getName");
    }

    /**
     * The bus type of the storage controller (IDE, SATA, SCSI, SAS or Floppy).
     *
     * @link https://www.virtualbox.org/sdkref/interface_i_storage_controller.html#a608d5b636e78c4b07af2dd07d9ef11a2
     * @return string
     */
    public function getBus() : string
    {
        return $this->call("IStorageController_getBus");
    }

    /**
     * The exact variant of storage controller hardware presented to the guest.
     *
     * For SCSI controllers, the default type is LsiLogic.
     *
     * @link https://www.virtualbox.org/sdkref/interface_i_storage_controller.html#ae1bd486ff9df74163e79d63dae96bd8c
     * @return string
     */
    public function getControllerType() : string
    {
        return $this->call('IStorageController_getControllerType');
    }

    public function getUseHostIOCache() : bool
    {
        return (bool) $this->call('IStorageController_getUseHostIOCache');
    }

    public function setUseHostIOCache(bool $use_host_io_cache)
    {
        $this->getMutable()->callRaw(
            "IStorageController_setUseHostIOCache",
            ["useHostIOCache" => $use_host_io_cache]
        );
        $this->save();
    }

    /**
     * The number of usable ports on the controller.
     * @return int
     */
    public function getPortCount() : int
    {
        return $this->call("IStorageController_getPortCount");
    }

    /**
     * Get the array of medium attachments.
     *
     * @return array<MediumAttachment>
     */
    public function getMediumAttachments() : array
    {
        return $this->getVM()->getMediumAttachmentOfController($this->getName());
    }

    /**
     * Attaches a device and optionally mounts a medium to this storage controller,
     * at the indicated port and device.
     *
     * If port is -1 (default), port will be automatically selected.
     *
     * @link https://www.virtualbox.org/sdkref/interface_i_machine.html#a8e51fafec7442a48a86a59edec5ec217
     *
     * @param Medium $medium
     * @param int $port
     * @param int $device for IDE controllers: 0 = master, 1 = slave, otherwise 0
     */
    public function attachDevice(Medium $medium, ?int $port = -1, ?int $device = 0)
    {
        if ($port === -1) {
            $port = $this->findAvailablePort();
        }

        $type = $medium->getDeviceType();
        return $this->getVM()->attachDevice($this->getName(), $port, $device, $type, $medium);
    }

    public function findAvailablePort() : int
    {
        $port_count = $this->getPortCount();
        $attachments = $this->getMediumAttachments();
        if (count($attachments) >= $port_count) {
            throw new \Exception("No available port!");
        }

        $used_ports = [];
        foreach ($attachments as $attachment) {
            /** @var MediumAttachment $attachment */
            $used_ports[] = $attachment->getPort();
        }

        $port = 0;
        while (in_array($port, $used_ports)) {
            $port++;
        }
        return $port;
    }

    /**
     * Detaches the device attached to the specified port and device slot.
     *
     * @link https://www.virtualbox.org/sdkref/interface_i_machine.html#a59a8e406027e901cc260488f8617fb22
     *
     * @param int $port
     * @param int $device for IDE controllers: 0 = master, 1 = slave, otherwise 0
     */
    public function detachDevice(int $port, ?int $device = 0)
    {
        return $this->getVM()->detachDevice($this->getName(), $port, $device);
    }
}
