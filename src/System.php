<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cylab\Vbox;

/**
 * Wrapper for ISystemProperties
 *
 * @author tibo
 */
class System extends VBoxObject
{

    /**
     * The max number of network adapters that can be attached to a VM.
     *
     * This depends on the emulated chipset, which can be PIIX3 (default) or
     * ICH9
     * @param string $chipset_type
     * @return int
     */
    public function getMaxNetworkAdapters(string $chipset_type = "PIIX3") : int
    {
        return $this->call("ISystemProperties_getMaxNetworkAdapters", [
            "chipset" => $chipset_type
        ]);
    }
}
