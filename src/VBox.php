<?php

namespace Cylab\Vbox;

/**
 * Description of VBox
 * https://www.virtualbox.org/sdkref/interface_i_virtual_box.html
 *
 * @author Thibault Debatty
 */
class VBox extends ManagedObject
{

    private $session;
    private $client = null;

    private $username;
    private $password;
    private $location;

    private $cache;


    /**
     *
     * @param string $username
     * @param string $password
     * @param string $location
     * @param bool $cache use caching of retrieved values (avoids repetitive SOAP requests)
     */
    public function __construct(
        string $username,
        string $password,
        string $location = "http://127.0.0.1:18083",
        bool $cache = true
    ) {

        $this->username = $username;
        $this->password = $password;
        $this->location = $location;
        $this->cache = $cache;
    }

    public function __destruct()
    {
        try {
            if ($this->getSession() !== null
                    && $this->getSession()->getState() == "Locked") {
                $this->getSession()->unlockMachine();
            }
        } catch (\SoapFault $ex) {
        }
    }

    /**
     * Are we using values caching?
     *
     * @return bool
     */
    public function cache() : bool
    {
        return $this->cache;
    }

    /**
     *
     * @return \SoapClient
     */
    public function getClient(): \SoapClient
    {
        if ($this->client == null) {
            ini_set('default_socket_timeout', "600");
            //$wsdl = __DIR__ . "/vboxweb-5.2.wsdl";
            $wsdl = __DIR__ . "/vboxweb-6.0.wsdl";
            $this->client = new \SoapClient(
                $wsdl,
                array(
                    'features' => (SOAP_USE_XSI_ARRAY_TYPE + SOAP_SINGLE_ELEMENT_ARRAYS),
                    'cache_wsdl' => WSDL_CACHE_BOTH,
                    'trace' => true,
                    'connection_timeout' => 20,
                    'location' => $this->location,
                    'keep_alive' => true)
            );

            $request = array(
                "username" => $this->username,
                "password" => $this->password
            );

            $response = $this->client->__soapCall(
                'IWebsessionManager_logon',
                array($request)
            );

            $this->uuid = $response->returnval;

            $request = array("refIVirtualBox" => $this->uuid);
            $response = $this->client->__soapCall(
                "IWebsessionManager_getSessionObject",
                array($request)
            );

            $this->session =  new Session($response->returnval, $this);
        }

        return $this->client;
    }

    public function getAPIVersion()
    {
        return $this->call("IVirtualBox_getAPIVersion");
    }

    /**
     *
     * @return Session
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     *
     * @return VM[]
     */
    public function allVMs() : array
    {

        $response = $this->callRaw('IVirtualBox_getMachines');
        if (!isset($response->returnval)) {
            return array();
        }

        $vms = array();
        foreach ($response->returnval as $uuid) {
            $vms[] = new VM($uuid, $this);
        }
        return $vms;
    }
    
    public function internalNetworks()
    {
        return $this->callRaw("IVirtualBox_getInternalNetworks");
    }
    
    /**
     * https://www.virtualbox.org/sdkref/interface_i_virtual_box.html#ae5b3671784688778c5f3f48fa0d50967
     * @return array
     */
    public function natNetworks() : array
    {
        $response = $this->callRaw('IVirtualBox_getNATNetworks');
        if (!isset($response->returnval)) {
            return [];
        }

        $vms = [];
        foreach ($response->returnval as $uuid) {
            $vms[] = new NATNetwork($uuid, $this);
        }
        return $vms;
    }
    
    public function createNATNetwork(string $name) : NATNetwork
    {
        $uuid = $this->call("IVirtualBox_createNATNetwork", [
            "networkName" => $name
        ]);
        
        return new NATNetwork($uuid, $this);
    }
    
    public function removeNATNetwork(NATNetwork $network) : void
    {
        $this->callRaw(
            "IVirtualBox_removeNATNetwork",
            ["network" => $network->uuid]
        );
    }

    /**
     *
     * @return Group[]
     */
    public function getGroups()
    {
        $groups = array();

        foreach ($this->allVMs() as $vm) {
            foreach ($vm->getGroups() as $group) {
                $group = Group::normalize($group);

                if (!in_array($group, $groups)) {
                    $groups[] = $group;
                }
            }
        }

        return array_map(function ($group_name) {
            return new Group($group_name, $this);
        }, $groups);
    }

    /**
     *
     * @param string $name
     * @return Group
     */
    public function getGroup(string $name)
    {
        return new Group($name, $this);
    }

    /**
     *
     * @param String $name name or id of VM
     * @return VM
     */
    public function findVM($name) : VM
    {
        return new VM(
            $this->call(
                "IVirtualBox_findMachine",
                array("nameOrId" => $name)
            ),
            $this
        );
    }

    /**
     * Create a virtual dhcp server for the specified internal network.
     * @param String $network_name
     * @return DHCPServer
     */
    public function createDHCPServer(String $network_name)
    {
        return new DHCPServer(
            $this->call(
                "IVirtualBox_createDHCPServer",
                ["name" => $network_name]
            ),
            $this
        );
    }

    public function getDHCPServers()
    {
        $response = $this->callRaw("IVirtualBox_getDHCPServers");
        if (!isset($response->returnval)) {
            return [];
        }

        $servers = [];
        foreach ($response->returnval as $uuid) {
            $servers[] = new DHCPServer($uuid, $this);
        }
        return $servers;
    }

    public function removeDHCPServer(DHCPServer $server)
    {
        $this->callRaw(
            "IVirtualBox_removeDHCPServer",
            ["server" => $server->uuid]
        );
    }
    
    public function findDHCPServerByNetworkName(string $name)
    {
        return new DHCPServer(
            $this->call(
                "IVirtualBox_findDHCPServerByNetworkName",
                ["name" => $name]
            ),
            $this
        );
    }

    /**
     * Create an appliance instance, which represents an ova file.
     * @return \Cylab\Vbox\Appliance
     */
    public function createAppliance() : Appliance
    {
        return new Appliance(
            $this->call('IVirtualBox_createAppliance'),
            $this
        );
    }

    /**
     * Import all VMs from an ova file.
     *
     * @param String $file
     * @return VM[]
     */
    public function import($file) : array
    {
        $appliance = $this->createAppliance();

        $progress = $appliance->read($file);
        $progress->waitForCompletion();
        $appliance->interpret();

        // change import directory to avoid collision
        $micro_date = microtime();
        $date_array = explode(" ", $micro_date);
        $date = date("Ymd-His", (int) $date_array[1]);
        $timestamp = $date . $date_array[0];
        foreach ($appliance->getVirtualSystemDescriptions() as $vsd) {
            $vsd->setBaseFolder($vsd->getBaseFolder() . "/$timestamp");
        }

        $progress = $appliance->importMachines();
        $progress->waitForCompletion();
        return $appliance->getMachines();
    }


    /**
     * Import multiple copies of the same OVA.
     * @param string $file
     * @param int $count
     * @return array<int, VM>
     * @throws \Exception
     */
    public function importMultiple(string $file, int $count = 1)
    {

        $appliances = [];
        $progresses = [];

        for ($i = 0; $i < $count; $i++) {
            $appliance = new Appliance(
                $this->call('IVirtualBox_createAppliance'),
                $this
            );

            $progress = $appliance->read($file);
            $progress->waitForCompletion();
            $appliance->interpret();

            foreach ($appliance->getVirtualSystemDescriptions() as $vsd) {
                $original_name = $vsd->getOriginalName();
                $tmp_name = $this->tmpName($original_name, $i);
                $vsd->setName($tmp_name);

                for ($disk_id = 0; $disk_id < $vsd->getHardDiskCount(); $disk_id++) {
                    $new_path = $this->convertPath(
                        $vsd->getHardDiskPath($disk_id),
                        $tmp_name
                    );
                    $vsd->setHardDiskPath($disk_id, $new_path);
                }
            }

            $appliances[] = $appliance;
            $progresses[] = $appliance->importMachines();
        }

        while (true) {
            $all_completed = true;

            foreach ($progresses as $progress) {
                if (! $progress->isCompleted()) {
                    $all_completed = false;
                    break;
                }

                if ($progress->getResultCode() != 0) {
                    throw new \Exception($progress->getError()->description());
                }
            }

            if ($all_completed) {
                break;
            }

            sleep(1);
        }

        /* @var $vms VM[] */
        $vms = [];
        foreach ($appliances as $appliance) {
            foreach ($appliance->getMachines() as $vm) {
                $vms[] = $vm;
            }
        }

        return $vms;
    }

    public function tmpName(string $original_name, int $i) : string
    {
        return date("Ymd.his") . "."
                . sprintf('%02d', $i) . "."
                . sprintf('%06d', rand(0, 999999)) . "."
                . preg_replace('/\s/', '-', strtolower($original_name));
    }

    /**
     * Convert something like /home/vbox/VMs/myvm/disk1.ova to
     * /home/vbox/VMs/$instance_name/disk1.ova
     * @param string $original_path
     * @param string $instance_name
     * @return string
     */
    public function convertPath(string $original_path, string $instance_name) : string
    {
        // Go 2 levels up
        $dir = dirname(dirname($original_path));
        $basename = basename($original_path);
        return $dir . "/$instance_name/" . $basename;
    }

    public function system() : System
    {
        return new System(
            $this->call("IVirtualBox_getSystemProperties"),
            $this
        );
    }

    public function host() : Host
    {
        return new Host(
            $this->call("IVirtualBox_getHost"),
            $this
        );
    }
    
    /**
     * Create a medium (harddisk or DVD).
     *
     * @param string $format vdi, vmdk etc.
     * @param string $location path on disk
     * @param string $accessMode ReadWrite or ReadOnly
     * @param string $type HardDisk, DVD etc.
     *
     * @link https://www.virtualbox.org/sdkref/interface_i_virtual_box.html#a9f5806720cd7e7f0ec8963230bf8a09d
     */
    public function createMedium(string $format, string $location, string $accessMode, string $type)
    {
        return new Medium(
            $this->call("IVirtualBox_createMedium", [
                    "format" => $format,
                    "location" => $location,
                    "accessMode" => $accessMode,
                    "aDeviceTypeType" => $type
            ]),
            $this
        );
    }
    
    /**
     * Array of medium objects known to this VirtualBox installation.
     *
     * @return array<Medium> list of harddisks
     *
     * @link https://www.virtualbox.org/sdkref/interface_i_virtual_box.html#a795457eb6321e42afb495064ee06213d
     */
    public function getHardDisks() : array
    {
        $disks = [];
        foreach ($this->call("IVirtualBox_getHardDisks") as $uuid) {
            $disks[] = new Medium($uuid, $this);
        }
        return $disks;
    }
    
    /**
     * Array of CD/DVD image objects currently in use by this VirtualBox instance.
     *
     * @link https://www.virtualbox.org/sdkref/interface_i_virtual_box.html#a840614ca6ed7428c755380f0b21f1f1e
     *
     * @return array<Medium>
     */
    public function getDVDImages() : array
    {
        $disks = [];
        
        $objects = $this->callRaw("IVirtualBox_getDVDImages")->returnval ?? [];
        foreach ($objects as $uuid) {
            $disks[] = new Medium($uuid, $this);
        }
        return $disks;
    }
    
    /**
     * Opens a medium from an existing storage location.
     *
     * @link https://www.virtualbox.org/sdkref/interface_i_virtual_box.html#a5336442d7ee9b95e4500b5f6e657bcb7
     *
     * @param string $location
     * @param string $type "HardDisk", "DVD" or "Floppy".
     * @param string $accessMode ReadOnly or ReadWrite
     * @param bool $forceNewUuid request a new medium UUID, even if the medium is already known to VirtualBox
     * @return Medium
     */
    public function openMedium(string $location, string $type, string $accessMode, bool $forceNewUuid) : Medium
    {
        return new Medium($this->call("IVirtualBox_openMedium", [
            "location" => $location,
            "deviceType" => $type,
            "accessMode" => $accessMode,
            "forceNewUuid" => $forceNewUuid
        ]), $this);
    }
    
    public function openDVD(string $location) : Medium
    {
        return $this->openMedium($location, "DVD", "ReadOnly", false);
    }
    
    /**
     * Shortcut method to create a virtual hard disk.
     *
     * @param string $location File location on disk, including extension (.vdi)
     * @param int $size Logical size, in Bytes
     */
    public function createHardDisk(string $location, int $size) : Medium
    {
        $medium = $this->createMedium(
            "vdi",
            $location,
            "ReadWrite",
            "HardDisk"
        );
        
        $medium->createBaseStorage(
            $size,
            ["Standard"]
        )->waitForCompletion();
        
        return $medium;
    }
}
