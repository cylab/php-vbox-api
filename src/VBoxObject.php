<?php
namespace Cylab\Vbox;

use Cylab\Vbox\VBox;

/**
 * A child element of VirtualBox (VM, Appliance, etc..)
 *
 * @author Thibault Debatty
 */
class VBoxObject extends ManagedObject
{

    /**
     *
     * @var VBox
     */
    private $vbox;

    public function __construct($uuid, VBox $vbox)
    {
        parent::__construct($uuid);
        $this->vbox = $vbox;
    }

    /**
     *
     * @return VBox
     */
    public function getVBox() : VBox
    {
        return $this->vbox;
    }

    /**
     *
     * @return \SoapClient
     */
    protected function getClient(): \SoapClient
    {
        return $this->getVBox()->getClient();
    }
}
