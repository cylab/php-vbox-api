<?php

namespace Cylab\Vbox;

/**
 * A single VM.
 * https://www.virtualbox.org/sdkref/_virtual_box_8idl.html#a80b08f71210afe16038e904a656ed9eb
 *
 * @author Thibault Debatty
 */
class VM extends VBoxObject
{

    const POWEREDOFF = "PoweredOff";
    const ABORTED = "Aborted";
    const SAVED = "Saved";
    const RUNNING = "Running";
    const PAUSED = "Paused";


    /**
     * The mutable equivalent of this VM
     * @var \Cylab\Vbox\VM | null
     */
    private $mutable;

    private $name = null;

    /**
     * Get the name of the VM.
     * @return String
     */
    public function getName() : string
    {

        if ($this->name !== null
                && $this->getVBox()->cache()) {
            return $this->name;
        }

        $this->name = $this->call("IMachine_getName");
        return $this->name;
    }

    /**
     * Set the name of the VM.
     * @param String $name
     */
    public function setName(string $name)
    {
        $this->getMutable()->callRaw("IMachine_setName", array("name" => $name));
        $this->save();
    }
    
    /**
     * Full name of the file containing machine settings data.
     *
     * @return string
     */
    public function getSettingsFilePath() : string
    {
        return $this->call("IMachine_getSettingsFilePath");
    }
    
    /**
     * Full name of the directory containing machine settings and disks
     * @return string
     */
    public function getSettingsDirectory() : string
    {
        return dirname($this->getSettingsFilePath());
    }

    /**
     *
     * @return \Cylab\Vbox\VM
     */
    public function getMutable() : VM
    {
        if ($this->mutable === null) {
            $this->lock();
            $this->mutable = $this->getVBox()->getSession()->getMachine();
        }

        return $this->mutable;
    }

    public function save()
    {
        $this->mutable->saveSettings();
        $this->mutable = null;
        $this->unlock();
    }

    public function unlock()
    {
        return $this->getVBox()->getSession()->unlockMachine();
    }

    public function getSessionState()
    {
        return $this->call("IMachine_getSessionState");
    }

    public function isLocked() : bool
    {
        return $this->getSessionState() === "Locked";
    }

    public function saveSettings()
    {
        $this->callRaw("IMachine_saveSettings");
    }

    const LOCK_WRITE = "Write";
    const LOCK_SHARED = "Shared";

    public function lock($type = self::LOCK_WRITE)
    {
        $this->callRaw(
            "IMachine_lockMachine",
            array(
                    "session" => $this->getVBox()->getSession()->getUUID(),
            "lockType" => $type)
        );
    }

    /**
     * Get the state of the VM (running, saved,...).
     * @return String
     */
    public function getState()
    {
        return $this->call("IMachine_getState");
    }

    public function isRunning() : bool
    {
        return $this->getState() == self::RUNNING;
    }

    /**
     * Methods corresponding to vboxweb api
     */
    public function launch()
    {
        $p = new Progress(
            $this->call("IMachine_launchVMProcess", array(
                    "session" => $this->getVBox()->getSession()->getUUID(),
                    "name" => "headless",
                    "environment" => "")),
            $this->getVBox()
        );
        $p->waitForCompletion();
        $this->unlock();
        sleep(1);
    }

    public function powerDown()
    {
        $this->lock(self::LOCK_SHARED);
        $p = new Progress(
            $this->getConsole()->call("IConsole_powerDown"),
            $this->getVBox()
        );
        $p->waitForCompletion();
        // $this->unlock();
        sleep(1);
    }

    /**
     * Start the VM (cold start or restore saved state).
     */
    public function up()
    {

        $state = $this->getState();
        if ($state == self::POWEREDOFF
                || $state == self::SAVED) {
            $this->launch();
        } elseif ($state == self::PAUSED) {
            $this->resume();
        }
    }

    public function pause()
    {
        $this->lock(self::LOCK_SHARED);
        $this->getConsole()->callRaw("IConsole_pause");
        $this->unlock();
        sleep(1);
    }

    /**
     * Press the reset button.
     */
    public function reset()
    {
        $this->lock(self::LOCK_SHARED);
        $this->getConsole()->callRaw("IConsole_reset");
        $this->unlock();
        sleep(1);
    }

    public function suspend()
    {
        $this->pause();

        // Save state
        $this->lock(self::LOCK_SHARED);
        $p = new Progress(
            $this->getVBox()->getSession()->getMachine()->call("IMachine_saveState"),
            $this->getVBox()
        );
        $p->waitForCompletion();
        sleep(1);
    }

    public function resume()
    {
        $this->lock(self::LOCK_SHARED);
        $this->getConsole()->callRaw("IConsole_resume");
        $this->unlock();
        sleep(1);
    }

    /**
     * Do a clean shutdown (ACPI shutdown, like pressing power button).
     */
    public function halt()
    {
        $this->lock(self::LOCK_SHARED);
        $this->getConsole()->callRaw("IConsole_powerButton");
        $this->unlock();
        sleep(1);
    }

    /**
     * Stop the VM immediately (hard shutdown, like holding the power button).
     */
    public function kill()
    {
        $this->powerDown();
    }

    /**
     * Destroy a machine, erasing all attached hard drives.
     */
    public function destroy()
    {
        $state = $this->getState();
        if ($state !== self::POWEREDOFF
                && $state !== self::ABORTED) {
            $this->kill();
        }

        $ret = $this->callRaw(
            "IMachine_unregister",
            ["cleanupMode" => "DetachAllReturnHardDisksOnly"]
        );

        // there is no returnval if the VM has no attached disk
        $media = $ret->returnval ?? [];
        
        $progress = new Progress(
            $this->call(
                "IMachine_deleteConfig",
                ["media" => $media]
            ),
            $this->getVBox()
        );
        $progress->waitForCompletion();
    }

    public function networkAdapters() : array
    {
        $max = $this->getVBox()->system()->getMaxNetworkAdapters();
        $adapters = [];
        for ($slot = 0; $slot < $max; $slot++) {
            $adapters[] = $this->getNetworkAdapter($slot);
        }
        return $adapters;
    }

    /**
     * Get the network adapter located at $slot (0..4).
     * @param int $slot
     * @return NetworkAdapter
     */
    public function getNetworkAdapter(int $slot)
    {

        if ($slot < 0) {
            throw new \Exception("Invalid slot id ($slot) : must be > 0");
        }

        return new NetworkAdapter(
            $this->call(
                "IMachine_getNetworkAdapter",
                array("slot" => $slot)
            ),
            $this
        );
    }

    public function getVRDEServer()
    {
        return new VRDEServer(
            $this->call("IMachine_getVRDEServer"),
            $this
        );
    }

    /**
     * Set the groups to which this machine belongs
     * @param String[] $groups
     */
    public function setGroups(array $groups)
    {
        $this->getMutable()->callRaw(
            "IMachine_setGroups",
            array("groups" => $groups)
        );

        $this->save();
    }

    /**
     *
     * @return String[]
     */
    public function getGroups() : array
    {
        return $this->call("IMachine_getGroups");
    }

    /**
     *
     * @return int
     */
    public function getCPUCount()
    {
        return $this->call("IMachine_getCPUCount");
    }

    /**
     * Set the number of CPU.
     * @param int $count
     */
    public function setCPUCount($count)
    {
        $this->getMutable()->callRaw(
            "IMachine_setCPUCount",
            array("CPUCount" => $count)
        );

        $this->save();
    }

    /**
     * Get CPU execution cap.
     *
     * The unit is percentage of host CPU cycles per second. The valid range is
     * 1 - 100. 100 (the default) implies no limit.
     * @return int
     */
    public function getCPUCap()
    {
        return $this->call("IMachine_getCPUExecutionCap");
    }

    /**
     * Set CPU execution cap.
     *
     * The unit is percentage of host CPU cycles per second. The valid range is
     * 1 - 100. 100 (the default) implies no limit.
     * @param int $cap
     */
    public function setCPUCap($cap)
    {
        $this->getMutable()->callRaw(
            "IMachine_setCPUExecutionCap",
            array("CPUExecutionCap" => $cap)
        );

        $this->save();
    }

    /**
     * Get RAM size.
     * @return int memory size in MB
     */
    public function getMemorySize()
    {
        return $this->call("IMachine_getMemorySize");
    }

    /**
     * Set RAM size.
     * @param int $value in MB
     */
    public function setMemorySize(int $value)
    {
        $this->getMutable()->callRaw(
            "IMachine_setMemorySize",
            ["memorySize" => $value]
        );
        $this->save();
    }

    /**
     * Get the UUID of the VM.
     * @return String
     */
    public function getUUID() : string
    {
        return $this->call("IMachine_getId");
    }

    public function __debugInfo()
    {
        return [
            'uuid' => $this->getUUID(),
            'name' => $this->getName(),
            'sessionState' => $this->getSessionState()
        ];
    }

    /**
     * Take a screenshot of the VM.
     *
     * Returns the binary PNG image, which can be saved with something like
     * file_put_contents('/tmp/image.png', $vm->takeScreenshot());
     *
     * @param int $max_width
     * @param int $max_height
     * @return string
     * @throws \Exception
     */
    public function takeScreenshot(
        int $max_width = 1600,
        int $max_height = 1200
    ) : string {
        $session = $this->getVBox()->getSession();
        $this->lock(self::LOCK_SHARED);
        try {
            $display = $session->getConsole()->getDisplay();
            $res = $display->computeResolution($max_width, $max_height);

            $data = $display->takeScreenshot(
                0,
                $res["width"],
                $res["height"],
                'PNG'
            );

            $this->unlock();
            return base64_decode($data);
        } catch (\Exception $e) {
            $this->unlock();
            throw  $e;
        }
    }
    
    public function getConsole() : Console
    {
        if (!$this->isLocked()) {
            throw new \Exception("VM must be locked before accessing console: "
                    . "https://www.virtualbox.org/sdkref/interface_i_console.html");
        }
        return $this->getVBox()->getSession()->getConsole();
    }

    /**
     * Get guest additions version.
     * VM must be running to call this method. Otherwize an Exception is
     * thrown.
     *
     * @return string
     * @throws \Exception
     */
    public function getGuestAdditionsVersion() : string
    {
        if ($this->getState() !== self::RUNNING) {
            throw new \Exception("VM must be running to acces guest");
        }
        $this->lock(self::LOCK_SHARED);
        $v = $this->getConsole()->getGuest()->getAdditionsVersion();
        $this->unlock();
        return $v;
    }

    public function getGuestFacilities()
    {
        if ($this->getState() !== self::RUNNING) {
            throw new \Exception("VM must be running to acces guest");
        }
        $this->lock(self::LOCK_SHARED);
        $v = $this->getConsole()->getGuest()->getFacilities();
        $this->unlock();
        return $v;
    }

    /**
     * Get statistics about the Guest (CPU, memory).
     * https://www.virtualbox.org/sdkref/interface_i_guest.html#a71c66f08f94b27598b9ce931f38559fd
     * Most values require GuestAdditions to be installed...
     *
     * class stdClass#368 (13) {
     *   public $cpuUser => int(0)
     *   public $cpuKernel =>  int(0)
     *   public $cpuIdle =>  int(0)
     *   public $memTotal =>  int(0)
     *   public $memFree =>  int(0)
     *   public $memBalloon =>  int(0)
     *   public $memShared =>  int(0)
     *   public $memCache =>  int(0)
     *   public $pagedTotal =>  int(0)
     *   public $memAllocTotal =>  int(314428)
     *   public $memFreeTotal =>  int(5060)
     *   public $memBalloonTotal =>  int(0)
     *   public $memSharedTotal =>  int(0)
     * }
     *
     * @throws \Exception
     */
    public function getGuestStatistics()
    {
        if ($this->getState() !== self::RUNNING) {
            throw new \Exception("VM must be running to acces guest");
        }
        $this->lock(self::LOCK_SHARED);
        $v = $this->getConsole()->getGuest()->internalGetStatistics();
        $this->unlock();
        return $v;
    }

    /**
     * Export this VM to a .ova
     * @param string $path
     */
    public function export(string $path)
    {
        $appliance = $this->getVBox()->createAppliance();
        $this->call("IMachine_exportTo", [
            "appliance" => $appliance->uuid,
            "location" => ""
        ]);
        $progress = $appliance->write($path);
        $progress->waitForCompletion();
    }
    
    // ------------- storage controlers, medium and attachments
    
    /**
     *
     * @return array<StorageController>
     */
    public function getStorageControllers() : array
    {
        $r = [];
        foreach ($this->call("IMachine_getStorageControllers") as $s) {
            $r[] = new StorageController($s, $this);
        }
        return $r;
    }

    public function getStorageControllerByName(string $name) : StorageController
    {
        return new StorageController($this->call(
            "IMachine_getStorageControllerByName",
            ["name" => $name]
        ), $this);
    }
    
    public function getMediumAttachmentOfController(string $name) : array
    {
        $r = [];
        $returns = $this->callRaw("IMachine_getMediumAttachmentsOfController", ["name" => $name])
                ->returnval ?? [];
        foreach ($returns as $obj) {
            $r[] = new MediumAttachment($obj, $this);
        }
        return $r;
    }

    public function getMediumAttachments() : array
    {
        $r = [];
        foreach ($this->call("IMachine_getMediumAttachments") as $obj) {
            $r[] = new MediumAttachment($obj, $this);
        }
        return $r;
    }
    
    /**
     * Attaches a device and optionally mounts a medium to the given storage controller
     * (identified by name), at the indicated port and device.
     *
     * @link https://www.virtualbox.org/sdkref/interface_i_machine.html#a8e51fafec7442a48a86a59edec5ec217
     *
     * @param string $name controller name
     * @param int $port
     * @param int $device for IDE controllers: 0 = master, 1 = slave, otherwise 0
     * @param string $type
     * @param Medium $medium
     */
    public function attachDevice(string $name, int $port, int $device, string $type, Medium $medium)
    {
        $this->getMutable()->callRaw("IMachine_attachDevice", [
            "name" => $name,
            "controllerPort" => $port,
            "device" => $device,
            "type" => $type,
            "medium" => $medium->getUUID()
        ]);
        $this->save();
    }
    
    /**
     * Detaches the device attached to a device slot of the specified controller
     * (identified by name).
     *
     * @link https://www.virtualbox.org/sdkref/interface_i_machine.html#a59a8e406027e901cc260488f8617fb22
     *
     * @param string $name controller name
     * @param int $port
     * @param int $device for IDE controllers: 0 = master, 1 = slave, otherwise 0
     */
    public function detachDevice(string $name, int $port, int $device)
    {
        $this->getMutable()->callRaw("IMachine_detachDevice", [
            "name" => $name,
            "controllerPort" => $port,
            "device" => $device
        ]);
        $this->save();
    }
}
