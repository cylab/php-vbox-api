<?php
namespace Cylab\Vbox;

use Cylab\Vbox\VM;

/**
 * A component of a VM (NIC, VRDEServer etc.).
 *
 * @author Thibault Debatty
 */
abstract class VMComponent extends ManagedObject
{

    /**
     *
     * @var VM
     */
    private $vm;

    public function __construct(string $uuid, VM $vm)
    {
        parent::__construct($uuid);

        $this->vm = $vm;
    }

    /**
     *
     * @return VM
     */
    public function getVM() : VM
    {
        return $this->vm;
    }

    /**
     *
     * @return \SoapClient
     */
    protected function getClient(): \SoapClient
    {
        return $this->getVM()->getVBox()->getClient();
    }

    abstract protected function getMutable();

    /**
     * Save the component after you made changes to the Mutable version of the
     * component.
     */
    protected function save()
    {
        $this->getVM()->save();
    }
}
