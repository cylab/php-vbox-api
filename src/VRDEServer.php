<?php

namespace Cylab\Vbox;

/**
 * Description of VRDEServer
 *
 * @author Thibault Debatty
 */
class VRDEServer extends VMComponent
{

    /**
     *
     * @return bool
     */
    public function isEnabled() : bool
    {
        return (bool) $this->call("IVRDEServer_getEnabled");
    }

    /**
     *
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled)
    {
        $this->getMutable()->callRaw(
            "IVRDEServer_setEnabled",
            array("enabled" => $enabled)
        );
        $this->save();
    }

    /**
     *
     * @return int
     */
    public function getPort() : int
    {
        return (int) $this->call(
            "IVRDEServer_getVRDEProperty",
            array("key" => "TCP/Ports")
        );
    }

    /**
     *
     * @param int $port
     */
    public function setPort(int $port)
    {
        $this->getMutable()->callRaw(
            "IVRDEServer_setVRDEProperty",
            array(
                    "key" => "TCP/Ports",
            "value" => $port)
        );
        $this->save();
    }

    /**
     *
     * @return string
     */
    public function getBindAddress() : string
    {
        return (string) $this->call(
            "IVRDEServer_getVRDEProperty",
            array("key" => "TCP/Address")
        );
    }

    public function setBindAddress(string $address)
    {
        $this->getMutable()->callRaw(
            "IVRDEServer_setVRDEProperty",
            array(
                    "key" => "TCP/Address",
            "value" => $address)
        );
        $this->save();
    }

    /**
     *
     * @return \Cylab\Vbox\VRDEServer
     */
    public function getMutable() : VRDEServer
    {
        return $this->getVM()->getMutable()->getVRDEServer();
    }
}
