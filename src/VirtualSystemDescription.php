<?php

namespace Cylab\Vbox;

/**
 * Description of a VM contained in an OVA, before it is imported.
 *
 * @author Thibault Debatty
 */
class VirtualSystemDescription extends VBoxObject
{

    /**
     * array(14) {
        [0] =>
        string(2) "OS"
        [1] =>
        string(4) "Name"
        [2] =>
        string(12) "PrimaryGroup"
        [3] =>
        string(12) "SettingsFile"
        [4] =>
        string(10) "BaseFolder"
        [5] =>
        string(3) "CPU"
        [6] =>
        string(6) "Memory"
        [7] =>
        string(9) "SoundCard"
        [8] =>
        string(13) "USBController"
        [9] =>
        string(14) "NetworkAdapter"
        [10] =>
        string(5) "CDROM"
        [11] =>
        string(21) "HardDiskControllerIDE"
        [12] =>
        string(21) "HardDiskControllerIDE"
        [13] =>
        string(13) "HardDiskImage"
      }
     * @var array
     */
    private $types;
    private $ova_values;
    private $vbox_values;
    private $extra_values;

    public function __construct($uuid, VBox $vbox)
    {
        parent::__construct($uuid, $vbox);

        $original_description = $this->callRaw("IVirtualSystemDescription_getDescription");
        $this->types = $original_description->types;
        $this->ova_values = $original_description->OVFValues;
        $this->vbox_values = $original_description->VBoxValues;
        $this->extra_values = $original_description->extraConfigValues;
    }

    /**
     *
     * @param string $type
     * @return int
     */
    public function findPosition(string $type) : int
    {
        return array_search($type, $this->types);
    }

    /**
     * Return all positions that contain an description of this types (for
     * example the position of all hard disk paths).
     * @param string $type
     * @return int[]
     */
    public function findPositions(string $type) : array
    {
        $positions = [];
        for ($i = 0; $i < count($this->types); $i++) {
            if ($this->types[$i] == $type) {
                $positions[] = $i;
            }
        }
        return $positions;
    }

    /**
     *
     * @return int
     */
    public function getHardDiskCount() : int
    {
        return count($this->getValuesForType("HardDiskImage"));
    }

    /**
     * Get the path of hard disk $id (0 is first hdd, 1 is second hdd etc.).
     * @param int $id
     * @return string
     */
    public function getHardDiskPath(int $id = 0)
    {
        return $this->getValuesForType("HardDiskImage")[$id];
    }

    public function setHardDiskPath(int $id, string $path)
    {
        $positions = $this->findPositions("HardDiskImage");
        $position = $positions[$id];
        $this->vbox_values[$position] = $path;
        $this->save();
    }

    public function getOriginalName()
    {
        return $this->getOriginalValueForType("Name");
    }

    public function getName()
    {
        return $this->getValueForType("Name");
    }

    public function setName($name)
    {
        $position = $this->findPosition("Name");
        $this->vbox_values[$position] = $name;
        $this->save();
    }

    public function getBaseFolder() : string
    {
        return $this->getValueForType("BaseFolder");
    }

    public function setBaseFolder(string $folder)
    {
        $position = $this->findPosition("BaseFolder");
        $this->vbox_values[$position] = $folder;
        $this->save();
    }

    /**
     * Return the first value of this type in the system description (for example,
     * the path of the first hard disk).
     * @param string $type
     * @return string
     */
    public function getValueForType($type)
    {
        $position = $this->findPosition($type);
        return $this->vbox_values[$position];
    }

    public function getValuesForType($type)
    {
        $positions = $this->findPositions($type);
        $r = [];
        foreach ($positions as $position) {
            $r[] = $this->vbox_values[$position];
        }
        return $r;
    }

    public function getOriginalValueForType($type)
    {
        $position = $this->findPosition($type);
        return $this->ova_values[$position];
    }

    public function save()
    {
        $enabled = $this->getEnabledArray();
        $this->callRaw("IVirtualSystemDescription_setFinalValues", [
            "enabled" => $enabled,
            "VBoxValues" => $this->vbox_values,
            "extraConfigValues" => $this->extra_values
        ]);
    }

    public function getEnabledArray()
    {
        $enabled = [];
        for ($i = 0; $i < count($this->types); $i++) {
            $enabled[$i] = true;
        }
        return $enabled;
    }
}
