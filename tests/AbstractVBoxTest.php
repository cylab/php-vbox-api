<?php

namespace Cylab\Vbox;

use PHPUnit\Framework\TestCase;

/**
 * Description of AbstractVBoxTest
 *
 * @author Thibault Debatty
 */
abstract class AbstractVBoxTest extends TestCase
{
    /**
     *
     * @var \Cylab\Vbox\VBox
     */
    private $vbox;

    protected function setUp() : void
    {
        $this->vbox = new VBox(
            getenv("VBOX_USER"),
            getenv("VBOX_PASSWORD"),
            getenv("VBOX_ADDRESS")
        );
    }

    /**
     * Get VBox instance.
     * @return \Cylab\Vbox\VBox
     */
    public function vbox()
    {
        return $this->vbox;
    }
}
