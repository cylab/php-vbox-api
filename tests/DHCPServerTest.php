<?php

namespace Cylab\Vbox;

/**
 * @group dhcp
 */
class DHCPServerTest extends AbstractVBoxTest
{

    public function testDHCPServer()
    {
        $vbox = $this->vbox();
        
        $servers = $vbox->getDHCPServers();
        foreach ($servers as $server) {
            $server->delete();
        }

        $this->assertEquals(0, count($vbox->getDHCPServers()));

        $dhcp = $vbox->createDHCPServer("internal_net");
        $dhcp->setConfiguration(
            "192.168.0.1",
            "255.255.255.0",
            "192.168.0.100",
            "192.168.0.200"
        );
        $dhcp->enable();
        $this->assertTrue($dhcp->isEnabled());
        
        $this->assertEquals("internal_net", $vbox->findDHCPServerByNetworkName("internal_net")->network());
        
        $this->assertEquals(1, count($vbox->getDHCPServers()));
        $this->assertEquals("internal_net", $vbox->getDHCPServers()[0]->network());

        $uuid = $dhcp->getUUID();
        $dhcp_copy = new DHCPServer($uuid, $vbox);
        
        $this->assertEquals("192.168.0.1", $dhcp_copy->ipAddress());
        $this->assertEquals("255.255.255.0", $dhcp_copy->networkMask());
        $this->assertEquals("192.168.0.100", $dhcp_copy->lowerIP());
        $this->assertEquals("192.168.0.200", $dhcp_copy->upperIP());
        
        $dhcp_copy->delete();

        $this->assertEquals(0, count($vbox->getDHCPServers()));
    }
}
