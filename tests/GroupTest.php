<?php

namespace Cylab\Vbox;

/**
 * @group vbox
 * @group vbox-groups
 */
class GroupTest extends AbstractVBoxTest
{

    /**
     * @var \Cylab\Vbox\VM
     */
    protected $vm;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() : void
    {
        parent::setUp();

        foreach ($this->vbox()->allVMs() as $vm) {
            $vm->destroy();
        }

        $this->vm = $this->vbox()->import(getenv("VBOX_ROOT") . "/dsl.ova")[0];
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown() : void
    {
        $this->vm->destroy();
    }

    /**
     * @covers Cylab\Vbox\Group::normalize
     */
    public function testNormalize()
    {
        $this->assertEquals("/", Group::normalize(""));
        $this->assertEquals("/", Group::normalize("/"));
        $this->assertEquals("/", Group::normalize("//"));
        $this->assertEquals("/", Group::normalize("/ /"));

        $this->assertEquals("/test/", Group::normalize("test"));
        $this->assertEquals("/test/", Group::normalize("/test"));
        $this->assertEquals("/test/", Group::normalize("/test/"));
        $this->assertEquals("/test/", Group::normalize("test/ "));
    }

    public function testGetVMs()
    {
        $this->vm->setGroups(array("/test/02"));
        $group = $this->vbox()->getGroup("test");
        $this->assertEquals(1, count($group->getVMs()));
        $this->assertEquals(0, count($group->getVMs(false)));
    }

    public function testGetGroups()
    {
        $this->vm->setGroups(array("/test/02", "/test0"));
        $groups = $this->vbox()->getGroups();

        ## /test0 and /test/02
        $this->assertEquals(2, count($groups));
    }
}
