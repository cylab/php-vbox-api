<?php

namespace Cylab\Vbox;

/**
 * Description of HostTest
 *
 * @group host
 * @author tibo
 */
class HostTest extends AbstractVBoxTest
{
    public function testProcessors()
    {
        $this->assertTrue(is_integer($this->vbox()->host()->processorCoreCount()));
    }

    public function testMemory()
    {
        var_dump($this->vbox()->host()->memorySize());
        var_dump($this->vbox()->host()->memoryAvailable());
    }

    public function testNetworkInterfaces()
    {
        foreach ($this->vbox()->host()->networkInterfaces() as $if) {
            echo $if->name() . ' ' . $if->ip() . ' ' . $if->mac() . ' '
                    . $if->status() . "\n";
        }
    }
}
