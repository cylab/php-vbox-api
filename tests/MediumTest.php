<?php

namespace Cylab\Vbox;

/**
 * Test virtual disks creation and management.
 *
 * @group medium
 * @author tibo
 */
class MediumTest extends AbstractVBoxTest
{
    /**
     * @var \Cylab\Vbox\VM
     */
    protected $vm;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->vm = $this->vbox()->import(getenv("VBOX_ROOT") . "/dsl.ova")[0];
    }

    protected function tearDown() : void
    {
        $this->vm->destroy();
    }
    
    
    public function testCreateMedium()
    {
        $vbox = $this->vbox();
        $disk_location = uniqid("/tmp/disk-") . ".vdi";
        
        $medium = $vbox->createMedium(
            "vdi",
            $disk_location,
            "ReadWrite",
            "HardDisk"
        );
        
        $progress = $medium->createBaseStorage(
            10 * 1024 * 1024 * 1024,
            ["Standard"]
        );
        
        $progress->waitForCompletion();
        
        // var_dump($medium->getName());
        // var_dump($medium->getSize());
        
        $this->assertEquals($disk_location, $medium->getLocation());
        $this->assertEquals(10 * 1024 * 1024 * 1024, $medium->getLogicalSize());
        
        $this->assertEquals("2MB", $medium->getSizeForHumans());
        $this->assertEquals("10GB", $medium->getLogicalSizeForHumans());
    }
    
    public function testHarddisks()
    {
        $vbox = $this->vbox();
        $disks = $vbox->getHardDisks();
        $init = count($disks);
        
        foreach ($disks as $disk) {
            /** @var Medium $disk */
            var_dump($disk->getLocation());
            var_dump($disk->getMachineIds());
        }
        
        $medium = $vbox->createHardDisk(uniqid("/tmp/disk-") . ".vdi", 1000 * 1000 * 1000);
        
        $this->assertEquals($init + 1, count($vbox->getHardDisks()));
    }
    
    public function testAttach()
    {
        $location = uniqid("/tmp/disk-") . ".vdi";
        $vbox = $this->vbox();
        $medium = $vbox->createHardDisk($location, 1000 * 1000 * 1000);
        
        $vm = $this->vm;
        /** @var StorageController $controller */
        $controller = $vm->getStorageControllers()[0];
        $this->assertEquals(2, $controller->getPortCount());
        
        // slave on port 0
        $controller->attachDevice($medium, 0, 1);
        
        foreach ($controller->getMediumAttachments() as $attachment) {
            /** @var MediumAttachment $attachment */
            if ($attachment->getPort() == 0 && $attachment->getDevice() == 1) {
                $this->assertEquals($location, $attachment->getMedium()->getLocation());
            }
        }
    }
    
    public function testDetach()
    {
        $vm = $this->vm;
        
        /** @var StorageController $controller */
        $controller = $vm->getStorageControllers()[0];
        $controller->detachDevice(1, 0);
        
        $this->assertEquals(1, $controller->findAvailablePort());
    }
    
    public function testAttachAtAvailablePort()
    {
        $location = uniqid("/tmp/disk-") . ".vdi";
        $vbox = $this->vbox();
        $medium = $vbox->createHardDisk($location, 1000 * 1000 * 1000);
        
        $vm = $this->vm;
        /** @var StorageController $controller */
        $controller = $vm->getStorageControllers()[0];
        
        $controller->detachDevice(1, 0);
        $controller->attachDevice($medium);
        
        foreach ($controller->getMediumAttachments() as $attachment) {
            /** @var MediumAttachment $attachment */
            if ($attachment->getPort() == 1 && $attachment->getDevice() == 0) {
                $this->assertEquals($location, $attachment->getMedium()->getLocation());
            }
        }
    }
    
    public function testDelete()
    {
        // create a new medium;
        $location = uniqid("/tmp/disk-") . ".vdi";
        $vbox = $this->vbox();
        $medium = $vbox->createHardDisk($location, 1000 * 1000 * 1000);
        
        foreach ($this->vbox()->getHardDisks() as $disk) {
            /** @var Medium $disk */
            if (! $disk->isAttached()) {
                $disk->delete()->waitForCompletion();
            }
        }
        
        // should only remain the disk of the test VM
        $this->assertEquals(1, count($vbox->getHardDisks()));
    }
    
    /**
     *
     * @group DVD
     */
    public function testDVDImages()
    {
        $vbox = $this->vbox();
        
        foreach ($vbox->getDVDImages() as $dvd) {
            $dvd->close();
        }
        $this->assertEquals(0, count($vbox->getDVDImages()));
        
        
        $vbox->openDVD(__DIR__ . "/TinyCore-15.iso");
        $this->assertEquals(1, count($vbox->getDVDImages()));
    }
}
