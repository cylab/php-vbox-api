<?php

namespace Cylab\Vbox;

use phpseclib\Net\SSH2;

/**
 * @group vbox
 */
class NetworkAdapterTest extends AbstractVBoxTest
{


    /**
     *
     * @var VM
     */
    private $vm;

    protected function setUp() : void
    {
        parent::setup();
        $this->vm = $this->vbox()->import(getenv("VBOX_ROOT") . "/dsl.ova")[0];
    }

    protected function tearDown() : void
    {
        $this->vm->destroy();
    }

    public function testGetAttachmentType()
    {
        $machine = $this->vm;
        $adapter = $machine->getNetworkAdapter(0);
        $adapter->setAttachmentType("NAT");
        $this->assertEquals(
            "NAT",
            $adapter->getAttachmentType()
        );
        $adapter->setAttachmentType("Bridged");
        $this->assertEquals(
            "Bridged",
            $adapter->getAttachmentType()
        );
    }

    public function testBridgedInterface()
    {

        $adapter = $this->vm->getNetworkAdapter(0);
        $adapter->setBridgedInterface("eth1");
        $this->assertEquals(
            "eth1",
            $adapter->getBridgedInterface()
        );
    }
    
    public function testBridgedInterface2()
    {
        $adapter = $this->vm->getNetworkAdapter(0);
        $adapter->setAttachmentType(NetworkAdapter::ATTACHEMENT_NAT);
        $adapter->bridge("eth1");
        
        $this->assertEquals("Bridged", $adapter->getAttachmentType());
        $this->assertEquals("eth1", $adapter->getNetworkName());
    }
    
    public function testInternalNetwork()
    {
        $adapter = $this->vm->getNetworkAdapter(0);
        $adapter->setAttachmentType(NetworkAdapter::ATTACHEMENT_INTERNAL);
        $adapter->setInternalNetwork("testnet01");
        
        $this->assertEquals("Internal", $adapter->getAttachmentType());
        $this->assertEquals("testnet01", $adapter->getNetworkName());
    }

    public function testGetSlot()
    {
        $machine = $this->vm;
        $adapter = $machine->getNetworkAdapter(0);
        $this->assertEquals(0, $adapter->getSlot());
        
        $this->assertNotEquals("", $adapter->getMACAddress());
    }
    
    /**
     * @group NAT
     */
    public function testNAT()
    {
        $adapter = $this->vm->getNetworkAdapter(0);
        $adapter->setEnabled(false);
        $this->assertEquals(false, $adapter->isEnabled());
        
        $adapter->setEnabled(true);
        $adapter->nat();
        $this->assertEquals("NAT", $adapter->getAttachmentType());
        
        $adapter->setNATNetwork("NAT01");
        $this->assertEquals("NAT01", $adapter->getNetworkName());
        
        $engine = $adapter->getNATEngine();
        // this is weird, but still..
        $this->assertEquals("", $engine->getNetwork());
        $engine->setNetwork("NAT02");
        $this->assertEquals("NAT02", $engine->getNetwork());
    }

    /**
     * @group NAT
     */
    public function testNATRedirect()
    {
        $adapter = $this->vm->getNetworkAdapter(0);
        $adapter->setAttachmentType(NetworkAdapter::ATTACHEMENT_NAT);

        $nat = $adapter->getNATEngine();
        $nat->addRedirect(new NATRedirect(2223, 22));

        $this->assertEquals(1, count($nat->getRedirects()));
        $redirect = $nat->getRedirects()[0];
        $this->assertEquals("tcp_2223_22", $redirect->name);

        $this->vm->up();
        sleep(5);
        $this->assertEquals("Running", $this->vm->getState());
    }

    /**
     * @group NATSSH
     */
    public function testSSH()
    {

        $vbox_ip = getenv("VBOX_SERVER");
        $vbox = $this->vbox();
        $ova = getenv("VBOX_ROOT") . "/ubuntu-20.04-server.ova";

        /* @var $vm VM */
        $vm = $vbox->import($ova)[0];
        $adapter = $vm->getNetworkAdapter(0);

        $adapter->setAttachmentType(NetworkAdapter::ATTACHEMENT_NAT);
        $nat = $adapter->getNATEngine();
        $nat->addRedirect(new NATRedirect(2225, 22, "TCP", $vbox_ip));

        $vm->up();
        sleep(60);
        $this->assertNotEquals("", $vm->getNetworkAdapter(0)->getIPAddress());

        $ssh = new SSH2($vbox_ip, 2225);
        if (!$ssh->login('vagrant', 'vagrant')) {
            throw new \Exception("Failed to login");
        }

        $this->assertEquals("vagrant", trim($ssh->exec('whoami')));

        $ssh->disconnect();
        $vm->destroy();
    }
}
