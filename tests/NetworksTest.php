<?php

namespace Cylab\Vbox;

/**
* @group networks
*/
class NetworksTest extends AbstractVBoxTest
{

    public function testNATNetworks()
    {
        $vbox = $this->vbox();
        
        foreach ($vbox->natNetworks() as $net) {
            $net->remove();
        }
        
        $net = $vbox->createNATNetwork("net01");
        $this->assertEquals("net01", $net->name());
        $this->assertEquals(1, count($vbox->natNetworks()));
        
        $this->assertTrue($net->isEnabled());
        $this->assertIsString($net->getNetwork());
        
        $net->setNetwork("192.168.155.0/24");
        $this->assertEquals("192.168.155.0/24", $net->getNetwork());
        $this->assertEquals("192.168.155.1", $net->gateway());
        $this->assertEquals(true, $net->hasDHCPServer());
    }
}
