<?php

namespace Cylab\Vbox;

/**
 * Description of StorageControllerTest
 *
 * @group storagecontroller
 *
 * @author tibo
 */
class StorageControllerTest extends AbstractVBoxTest
{
    /**
     * @var \Cylab\Vbox\VM
     */
    protected $vm;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->vm = $this->vbox()->import(getenv("VBOX_ROOT") . "/dsl.ova")[0];
    }

    protected function tearDown() : void
    {
        $this->vm->destroy();
    }

    public function testStorageController()
    {
        foreach ($this->vm->getStorageControllers() as $c) {
            /** @var StorageController $c */
            $this->assertEquals("IDE", $c->getName());
            $this->assertEquals("PIIX4", $c->getControllerType());
            $this->assertEquals(true, $c->getUseHostIOCache());
        }

        $controller = $this->vm->getStorageControllers()[0];
        /** @var StorageController $controller */
        $controller->setUseHostIOCache(false);
        $this->assertEquals(false, $controller->getUseHostIOCache());
    }
}
