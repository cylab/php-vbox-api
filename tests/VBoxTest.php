<?php

namespace Cylab\Vbox;

/**
 * @group vbox
 */
class VBoxTest extends AbstractVBoxTest
{

    /**
     * @group vbox-version
     */
    public function testAPIVersion()
    {
        $this->assertEquals(
            getenv("VBOX_VERSION"),
            $this->vbox()->getAPIVersion()
        );
    }

    public function testGetSession()
    {
        $this->vbox()->getClient();
        $this->assertTrue($this->vbox()->getSession()->getUUID() != "");
    }


    public function testLock()
    {
        $this->vbox()->import(getenv("VBOX_ROOT") . "/dsl.ova");
        $vms = $this->vbox()->allVMs();
        $vm = end($vms);
        $vm->lock();
        $this->assertEquals("Locked", $vm->getSessionState());
        $this->assertEquals("Locked", $this->vbox()->getSession()->getState());
        $vm->unlock();
        $vm->destroy();
    }

    public function testImportDestroy()
    {
        $init = count($this->vbox()->allVMs());
        $machines = $this->vbox()->import(getenv("VBOX_ROOT") . "/dsl.ova");
        $this->assertEquals("Cylab\Vbox\VM", get_class($machines[0]));
        $this->assertEquals("PoweredOff", $machines[0]->getState());
        $this->assertEquals($init + 1, count($this->vbox()->allVMs()));

        end($machines)->destroy();
        $this->assertEquals($init, count($this->vbox()->allVMs()));
    }

    public function testSetName()
    {
        $this->vbox()->import(getenv("VBOX_ROOT") . "/dsl.ova");
        $vms = $this->vbox()->allVMs();
        /* @var $vm \VM */
        $vm = end($vms);
        $name = $this->generateRandomString(30);
        $vm->setName($name);
        $this->assertEquals($name, $vm->getName());
        // second time, the name is fetched from local cache...
        $this->assertEquals($name, $vm->getName());
        $vm->destroy();
    }

    public function testGroups()
    {
        /* @var $vm \VM */
        $vm = $this->vbox()->import(getenv("VBOX_ROOT") . "/dsl.ova")[0];
        $vm->setGroups(array("/my-new-group"));
        $this->assertEquals("/my-new-group", $vm->getGroups()[0]);
        $vm->destroy();
    }

    public function testModifyDiskPath()
    {
        $path = "/home/vbox/VirtualBox VMs/Damn Small Linux_54/disk1.vmdk";
        $expected = "/home/vbox/VirtualBox VMs/20180829.082100.01.123/disk1.vmdk";
        $this->assertEquals(
            $expected,
            $this->vbox()->convertPath($path, "20180829.082100.01.123")
        );
    }

    /**
     * @group import-multiple
     */
    public function testImportMultiple()
    {
        $initial = count($this->vbox()->allVMs());
        $vms = $this->vbox()->importMultiple(getenv("VBOX_ROOT") . "/dsl.ova", 4);
        $this->assertEquals(4, count($vms));
        $this->assertEquals($initial + 4, count($this->vbox()->allVMs()));

        foreach ($vms as $vm) {
            $vm->destroy();
        }
    }

    /**
     * @group rdp
     */
    public function testVRDPort()
    {
        $port = 3391;
        /* @var $vm \Cylab\Vbox\VM */
        $vm = $this->vbox()->import(getenv("VBOX_ROOT") . "/dsl.ova")[0];
        $vrde = $vm->getVRDEServer();

        $vrde->setPort($port);
        $this->assertEquals($port, $vrde->getPort());

        $vrde->setEnabled(true);
        $this->assertTrue($vrde->isEnabled());

        $vrde->setEnabled(false);
        $this->assertFalse($vrde->isEnabled());
        $vm->destroy();
    }

    /**
     * @group rdp
     */
    public function testVRDPBindAddress()
    {
        $address = '192.168.0.1';
        $vm = $this->vbox()->import(getenv("VBOX_ROOT") . "/dsl.ova")[0];
        $vrde = $vm->getVRDEServer();

        $vrde->setBindAddress($address);
        $this->assertEquals($address, $vrde->getBindAddress());
    }


    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @group system-properties
     */
    public function testSystemProperties()
    {
        $this->assertEquals(8, $this->vbox()->system()->getMaxNetworkAdapters());
    }
}
