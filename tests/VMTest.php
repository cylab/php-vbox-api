<?php

namespace Cylab\Vbox;

/**
* @group vbox
*/
class VMTest extends AbstractVBoxTest
{
    /**
     * @var \Cylab\Vbox\VM
     */
    protected $vm;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->vm = $this->vbox()->import(getenv("VBOX_ROOT") . "/dsl.ova")[0];
    }

    protected function tearDown() : void
    {
        $this->vm->destroy();
    }

    public function testUUID()
    {
        $this->assertNotEquals("", $this->vm->getUUID());
    }

    /**
     * @group const
     */
    public function testConstants()
    {
        $this->assertEquals("Running", VM::RUNNING);
    }

    public function testCPUCount()
    {
        $this->vm->setCPUCount(2);
        $this->assertEquals(2, $this->vm->getCPUCount());
    }

    public function testCPUCap()
    {
        $this->vm->setCPUCap(50);
        $this->assertEquals(50, $this->vm->getCPUCap());
    }

    public function testMemorySize()
    {
        $this->vm->setMemorySize(512);
        $this->assertEquals(512, $this->vm->getMemorySize());
    }

    public function testChangeState()
    {
        $vm = $this->vm;

        $vm->up();
        $this->assertEquals("Running", $vm->getState());

        $vm->pause();
        $this->assertEquals("Paused", $vm->getState());

        // Test up after pause...
        $vm->up();
        $this->assertEquals("Running", $vm->getState());

        $vm->pause();
        $this->assertEquals("Paused", $vm->getState());

        $vm->resume();
        $this->assertEquals("Running", $vm->getState());

        $vm->suspend();
        $this->assertEquals("Saved", $vm->getState());

        $vm->up();
        $this->assertEquals("Running", $vm->getState());

        $vm->reset();
        $this->assertEquals("Running", $vm->getState());

        $vm->kill();
        $this->assertEquals("PoweredOff", $vm->getState());
    }

    /**
     * @group export
     */
    public function testExport()
    {
        $file = tempnam("/tmp", "VM-") . ".ova";
        $this->vm->export($file);
    }

    /**
     * @group session-state
     */
    public function testSessionState()
    {
        $this->assertEquals("Unlocked", $this->vm->getSessionState());
        $this->assertEquals(false, $this->vm->isLocked());
        $this->vm->lock(VM::LOCK_SHARED);
        $this->assertEquals(true, $this->vm->isLocked());
        $this->vm->unlock();
    }

    /**
     * Simply test nothing crashes when we take a screenshot.
     * @group screenshot
     */
    public function testScreenshot()
    {
        $vm = $this->vm;
        $vm->up();
        sleep(5);

        $data = $vm->takeScreenshot();
        $this->assertNotNull($data);
        $this->assertNotEquals("", $data);
        file_put_contents('/tmp/screenshot01.png', $data);

        $data = $vm->takeScreenshot(1600, 300);
        $this->assertNotNull($data);
        $this->assertNotEquals("", $data);
        file_put_contents('/tmp/screenshot02.png', $data);
    }

    /**
     * @group guest-additions
     */
    public function testGuestAdditions()
    {
        $this->vm->up();
        sleep(5);
        var_dump($this->vm->getGuestAdditionsVersion());
        var_dump($this->vm->getGuestFacilities());
    }


    /**
     * @group guest-statistics
     */
    public function testGuestStatistics()
    {
        $this->vm->up();
        sleep(5);
        var_dump($this->vm->getGuestStatistics());
    }

    /**
     * @group vm-network-adapters
     */
    public function testNetworkAdapters()
    {
        $this->assertEquals(8, count($this->vm->networkAdapters()));
    }

    /**
     * @group vm-media
     */
    public function testMedia()
    {
        foreach ($this->vm->getMediumAttachments() as $attachment) {
            if (! $attachment->hasMedium()) {
                continue;
            }
            $medium = $attachment->getMedium();
            $this->assertEquals("DSL-disk1.vmdk", $medium->getName());
            $this->assertEquals(32505856, $medium->getSize());
        }
    }
}
